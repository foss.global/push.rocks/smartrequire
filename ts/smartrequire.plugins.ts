import 'typings-global'
import * as resolve from 'resolve'
import * as smartq from 'smartq'

export {
  resolve,
  smartq
}
