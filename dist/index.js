"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const plugins = require("./smartrequire.plugins");
// resolving
/**
 * resolve a module path
 * @executes ASYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
exports.resolveInDir = (moduleNameArg, dirNameArg = process.cwd()) => __awaiter(this, void 0, void 0, function* () {
    let done = plugins.smartq.defer();
    plugins.resolve(moduleNameArg, { basedir: dirNameArg }, function (err, resolvedPath) {
        if (err) {
            done.reject(err);
            return;
        }
        done.resolve(resolvedPath);
    });
    return yield done.promise;
});
/**
 * resolve a module path in a directory
 * @executes SYNC
 * @param moduleNameArg
 * @param dirNameArg
 */
exports.resolveInDirSync = (moduleNameArg, dirNameArg = process.cwd()) => {
    return plugins.resolve.sync(moduleNameArg, { basedir: dirNameArg });
};
exports.requireInDir = (moduleNameArg, dirNameArg = process.cwd()) => __awaiter(this, void 0, void 0, function* () {
    let resolvedPath = yield exports.resolveInDir(moduleNameArg, dirNameArg);
    let module = require(resolvedPath);
    return module;
});
exports.requireInDirSync = (moduleNameArg, dirNameArg = process.cwd()) => {
    let resolvedPath = exports.resolveInDirSync(moduleNameArg, dirNameArg);
    let module = require(resolvedPath);
    return module;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsa0RBQWlEO0FBRWpELFlBQVk7QUFFWjs7Ozs7R0FLRztBQUNRLFFBQUEsWUFBWSxHQUFHLENBQU8sYUFBcUIsRUFBRSxhQUFxQixPQUFPLENBQUMsR0FBRyxFQUFFO0lBQ3hGLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFVLENBQUE7SUFDekMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLEVBQUUsVUFBVSxHQUFHLEVBQUUsWUFBWTtRQUNqRixFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNoQixNQUFNLENBQUE7UUFDUixDQUFDO1FBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUM1QixDQUFDLENBQUMsQ0FBQTtJQUNGLE1BQU0sQ0FBQyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUE7QUFDM0IsQ0FBQyxDQUFBLENBQUE7QUFFRDs7Ozs7R0FLRztBQUNRLFFBQUEsZ0JBQWdCLEdBQUcsQ0FBQyxhQUFxQixFQUFFLGFBQXFCLE9BQU8sQ0FBQyxHQUFHLEVBQUU7SUFDdEYsTUFBTSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFBO0FBQ3JFLENBQUMsQ0FBQTtBQUVVLFFBQUEsWUFBWSxHQUFHLENBQU8sYUFBYSxFQUFFLFVBQVUsR0FBRyxPQUFPLENBQUMsR0FBRyxFQUFFO0lBQ3hFLElBQUksWUFBWSxHQUFHLE1BQU0sb0JBQVksQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUE7SUFDaEUsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFBO0lBQ2xDLE1BQU0sQ0FBQyxNQUFNLENBQUE7QUFDZixDQUFDLENBQUEsQ0FBQTtBQUVVLFFBQUEsZ0JBQWdCLEdBQUcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxHQUFHLE9BQU8sQ0FBQyxHQUFHLEVBQUU7SUFDdEUsSUFBSSxZQUFZLEdBQUcsd0JBQWdCLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxDQUFBO0lBQzlELElBQUksTUFBTSxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQTtJQUNsQyxNQUFNLENBQUMsTUFBTSxDQUFBO0FBQ2YsQ0FBQyxDQUFBIn0=